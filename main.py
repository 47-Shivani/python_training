from flask import Flask, jsonify, render_template, request, make_response, request
import secrets
import string
import hashlib
import psycopg2.extras
from functools import wraps
import datetime
import jwt
from flask_mail import Mail, Message
from functools import wraps
import csv
import redis_task
from io import TextIOWrapper
from minio import Minio




app = Flask(__name__)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'shivaniamodsah@gmail.com'
app.config['MAIL_PASSWORD'] = ''
app.config['MAIL_USE_SSL'] = True
postmess = Mail(app)

app.config['SECRET_KEY'] = "this"
students = []
con = psycopg2.connect(database="postgres", user="postgres", password="password", host="172.17.0.1", port="5432")
cur = con.cursor()
token = None
r = redis_task.Redis(host='172.17.0.1', db='0')





def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            JWT_ALGORITHM = 'HS256'
            data = jwt.decode(token, app.config['SECRET_KEY'], JWT_ALGORITHM)
            current_user = data['user']
            user_type = data['user_type']
        except Exception as e:

            print(f"Error message: {e}")
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(current_user, user_type, *args, **kwargs)

    return decorated


@app.route('/delete', methods=['DELETE'])
@token_required
def delete(*args):
    user_type = args[1]
    current_user = args[0]

    if user_type == 'admin':
        if "email" not in request.json:
            cur.execute(f"delete from teacher WHERE email = %s", [current_user])
            if r.exists(current_user):
                r.delete(current_user)
            con.commit()
            return make_response('delete your data', 401, {'www-authenticate': ' basic realm="login required"'})

        data = request.get_json()
        email = data['email']
        role_type = data['role_type']

        if role_type == 'teacher' or role_type == 'admin':
            cur.execute("SELECT * FROM teacher WHERE email = %s and role_type='teacher'", [email])
        else:
            cur.execute("SELECT * FROM student WHERE email = %s", [email])

        user_data = cur.fetchone()
        if user_data:
            if role_type == 'admin':
                role_type = 'teacher'
            if r.exists(email):
                r.delete(email)
            cur.execute(f"delete from {role_type} WHERE email = %s", [email])

        else:
            return "please enter correct email id"

    else:
        cur.execute(f"delete from {user_type} WHERE email = %s", [current_user])
        if r.exists(current_user):
            r.delete(current_user)
        con.commit()
        count = cur.rowcount
        print(count, "Record delete successfully ")
        return make_response('delete your data', 401, {'www-authenticate': ' basic realm="login required"'})

@app.route('/update', methods=['PUT'])
@token_required
def update(*args):
    user_type = args[1]
    current_user = args[0]
    if "email" in request.json:
        if user_type == 'admin':
            data = request.get_json()
            email = data['email']
            role_type = data['role_type']
            if role_type == 'teacher' or role_type == 'admin':
                cur.execute("SELECT * FROM teacher WHERE email = %s", [email])
            else:
                cur.execute("SELECT * FROM student WHERE email = %s", [email])

            user_data = cur.fetchone()
            if user_data:
                if 'address' in request.json:
                    cur.execute(f"UPDATE {role_type} SET address=%s WHERE email = %s", (request.json['address'], email))
                if 'name' in request.json:
                    cur.execute(f"UPDATE {role_type} SET name=%s WHERE email = %s", (request.json['name'], email))
                if 'mobile' in request.json:
                    cur.execute(f"UPDATE {role_type} SET mobile=%s WHERE email = %s",
                                (request.json['mobile'], email))

            else:
                return "please enter correct email id"
    else:
        if user_type == 'admin':
            user_type = "teacher"
        if 'address' in request.json:
            cur.execute(f"UPDATE {user_type} SET address=%s WHERE email = %s", (request.json['address'], current_user))
        if 'name' in request.json:
            cur.execute(f"UPDATE {user_type} SET name=%s WHERE email = %s", (request.json['name'], current_user))
        if 'mobile' in request.json:
            cur.execute(f"UPDATE {user_type} SET mobile=%s WHERE email = %s", (request.json['mobile'], current_user))

    con.commit()
    count = cur.rowcount
    print(count, "Record Updated successfully ")
    return make_response('update your data', 401, {'www-authenticate': ' basic realm="login required"'})


@app.route('/get_data')
@token_required
def show_data(*args):
    user_type = args[1]
    current_user = args[0]
    if user_type == 'teacher' or user_type == 'admin':
        cur.execute("SELECT * FROM teacher WHERE email = %s", [current_user])
    else:
        cur.execute("SELECT * FROM student WHERE email = %s", [current_user])

    user_data = cur.fetchall()
    data = []
    for col in user_data:
        data = {
            'name': col[4],
            'address': col[5],
            'contact_number': col[6]
        }

    return jsonify({'information': data})


@app.route('/teachersdata', methods=['GET'])
@token_required
def get_teachers_data(*args):
    cur.execute("SELECT * FROM teacher where role_type='admin'")
    user_data = cur.fetchall()
    data = []
    for col in user_data:
        data.append({
            'name': col[4],
            'address': col[5],
            'contact_number': col[6]
        })

    return jsonify({'information1': data})


@app.route('/studentsdata', methods=['GET'])
@token_required
def get_students_data(*args):
    cur.execute("SELECT * FROM student")
    user_data = cur.fetchall()
    data = []
    for col in user_data:
        data.append({
            'name': col[4],
            'address': col[5],
            'contact_number': col[6]
        })

    return jsonify({'information': data})


@app.route('/register', methods=['POST'])
def register(*args):
    data = request.get_json()
    email = data['email']
    password = data['password']
    name = data['name']
    address = data['address']
    mobile = data['mobile']
    role_type = data['role_type']

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    if role_type == 'student':
        sql = "INSERT INTO student (email,password,salt,name,address,mobile) VALUES(%s,%s,%s,%s,%s,%s)"
        cur.execute(sql, (email, hashed, salt_value, name, address, mobile))

    else:
        sql = "INSERT INTO teacher (email,password,salt,name,address,mobile,role_type) VALUES(%s,%s,%s,%s,%s,%s,%s)"
        cur.execute(sql, (email, hashed, salt_value, name, address, mobile, role_type))

    con.commit()
    return f"{name} register successfully"


@app.route('/teacher_login', methods=['POST'])
def teacher_login():
    return login('teacher')


@app.route('/admin_login', methods=['POST'])
def admin_login():
    return login('admin')


@app.route('/student_login', methods=['POST'])
def login(user_type='student'):
    print(user_type)
    data = request.get_json()
    email = data['email']
    password = data['password']

    if user_type == 'admin' or user_type == 'teacher':
        sql = f"select * from teacher where email='{email}'"

    else:
        sql = f"select * from student where email='{email}'"

    cur.execute(sql)
    row = cur.fetchone()
    if row is not None:

        hashed = row[2]
        salt = row[3]
        user = row[1]

        user_pass = password + salt

        user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

        if hashed == user_pass:
            token = jwt.encode({'user': user, 'user_type': user_type,
                                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=10)},
                               app.config['SECRET_KEY'], algorithm="HS256")

            return jsonify({'token': token})
        else:

            return jsonify({'message': 'enter right password'})

    return jsonify({'message': 'enter right id'})


@app.route('/update_password', methods=["POST", "GET"])
@token_required
def update_password(current_user, user_type):
    data = request.get_json()
    password = data['password']
    new_password = data['new_password']
    if user_type == 'admin' or user_type == 'teacher':
        sql = f"select * from teacher where email='{current_user}'"

    else:
        sql = f"select * from student where email='{current_user}'"

    cur.execute(sql)
    row = cur.fetchone()
    if row is not None:

        hashed = row[2]
        salt = row[3]

        user_pass = password + salt

        user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

        if hashed == user_pass:
            new_password = new_password + salt
            hashed = hashlib.sha256(new_password.encode()).hexdigest()

            if user_type == 'teacher' or user_type == 'admin':
                sql = f"UPDATE teacher SET password ='{hashed}' WHERE email='{current_user}'"
            else:
                sql = f"UPDATE student SET password ='{hashed}' WHERE email='{current_user}'"

            cur.execute(sql)
            con.commit()
            count = cur.rowcount
            print(count, "Record Updated successfully ")

    return jsonify({'message': 'password update successfully'})


@app.route('/teacher_forgot', methods=['POST'])
def teacher_forgot():
    return forgot('teacher')


@app.route('/admin_forgot', methods=['POST'])
def admin_forgot():
    return forgot('admin')


@app.route('/student_forgot', methods=["POST", "GET"])
def forgot(user_type='student'):
    data = request.get_json()
    user = data['email']
    if user_type == 'teacher' or user_type == 'admin':
        sql = f"select * from teacher where email='{user}'"
    else:
        sql = f"select * from student where email='{user}'"
    cur.execute(sql)
    row = cur.fetchone()
    if row is not None:
        msg = Message('Confirm Password Change', sender='simranpatidar16@gmail.com', recipients=[user])
        msg.body = "Hello,\nWe've received a request to reset your password. If you want to reset your password, " \
                   "click the link below and enter your new password\n http://localhost:5000/setpassword/" + \
                   user_type + "/" + user
        postmess.send(msg)
        return "check your email"

    else:
        return "enter correct email"


@app.route('/setpassword/<string:user_type>/<string:user>', methods=["POST", "GET"])
def setpassword(user_type, user):
    data = request.get_json()
    password = data['password']

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    if user_type == 'teacher' or user_type == 'admin':

        sql = f"UPDATE teacher SET password ='{hashed}',salt ='{salt_value}' WHERE email='{user}'"
    else:
        sql = f"UPDATE student SET password ='{hashed}',salt ='{salt_value}' WHERE email='{user}'"

    cur.execute(sql)
    con.commit()

    return "your password change successfully"


@app.route('/')
def index():
    return "hello student"


@app.route("/students", methods=['GET'])
def get():
    cur.execute("select * from flasktable")
    students1 = cur.fetchall()
    for row in students1:
        student = {
            'id': row[0],
            'email': row[2],
            'password': row[1],
            'name': row[2],
            'address': row[4],
            'mobile': row[3]
        }
        students.append(student)
    return jsonify(students)


@app.route('/students/<int:id>', methods=['GET'])
def get_student(id):
    return jsonify({'student': students[id]})


@app.route('/add')
def add_student():
    return render_template('StudentsForm.html')


@app.route('/uploadmarks', methods=['POST'])
@token_required
def uploadmarks(*args):
    csv_file = request.files['file']
    csv_file = TextIOWrapper(csv_file, encoding='utf-8')
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        email, subject, total_marks, obtain_marks, *other = row
        sql = "INSERT INTO marks (email,subject,total_marks,obtain_marks) VALUES (%s,%s,%s,%s)"
        cur.execute(sql, [email, subject, total_marks, obtain_marks])
    con.commit()

    return "upload marks successfully"


@app.route('/checkmarks', methods=['POST'])
@token_required
def checkmarks(*args):
    user_type = args[1]
    current_user = args[0]
    if user_type == 'student':
        cur.execute("SELECT * FROM marks WHERE email = %s", [current_user])
        print(current_user)
        user_data = cur.fetchall()
        data = []
        if user_data:
            for col in user_data:
                data.append({
                    'subject': col[2],
                    'total_marks': col[3],
                    'obtain_marks': col[4]
                })
        else:
            return "marks is not available"
    else:
        return "unauthorized user"

    return jsonify({'information': data})


@app.route('/', methods=['POST'])
def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)
    for _ in range(6):
        password += secrets.choice(string_source)
    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
