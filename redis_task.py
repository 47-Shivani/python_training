import csv
import datetime
import hashlib
import secrets
import string
from functools import wraps
from io import TextIOWrapper
import jwt

import psycopg2
import redis

from flask import Flask, jsonify, make_response, request

app = Flask(__name__)

app.config['SECRET_KEY'] = "this"
con = psycopg2.connect("host=172.17.0.1 dbname=postgres user=postgres password=password")
cur = con.cursor()
r = redis.Redis(host='172.17.0.1',port='6379' ,db='0')


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'})

        try:
            message = "unauthorized user"
            jwt_algorithm = 'HS256'
            message = "unauthorized user"
            data = jwt.decode(token, app.config['SECRET_KEY'], jwt_algorithm)
            current_user = data['user']
            user_type = data['user_type']

            admin_role = ["register", "get_students_data",
                          "get_teachers_data"]
            teacher_role = "uploadmarks"

            fun_name = f.__name__

            if fun_name in admin_role and user_type != 'admin':
                return message
            if fun_name == teacher_role and user_type != 'teacher':
                return message
            if fun_name == 'checkmarks' and user_type != 'student':
                return message

        except Exception as e:

            print(f"Error message: {e}")
            return jsonify({'message': 'please login first'})

        return f(current_user, user_type, *args, **kwargs)

    return decorated


@app.route('/')
def index():
    return "hello user"


@app.route('/delete', methods=['DELETE'])
@token_required
def delete(*args):
    user_type = args[1]
    current_user = args[0]

    if user_type == 'admin':
        if "email" not in request.json:
            cur.execute(f"delete from teacher WHERE email = %s", [current_user])
            if r.exists(current_user):
                r.delete(current_user)
            con.commit()
            return make_response('delete your data', 401, {'www-authenticate': ' basic realm="login required"'})

        data = request.get_json()
        email = data['email']
        role_type = data['role_type']

        if role_type == 'teacher' or role_type == 'admin':
            cur.execute("SELECT * FROM teacher WHERE email = %s and role_type='teacher'", [email])
        else:
            cur.execute("SELECT * FROM student WHERE email = %s", [email])

        user_data = cur.fetchone()
        if user_data:
            if role_type == 'admin':
                role_type = 'teacher'
            if r.exists(email):
                r.delete(email)
            cur.execute(f"delete from {role_type} WHERE email = %s", [email])

        else:
            return "please enter correct email id"

    else:
        cur.execute(f"delete from {user_type} WHERE email = %s", [current_user])
        if r.exists(current_user):
            r.delete(current_user)
        con.commit()
        count = cur.rowcount
        print(count, "Record delete successfully ")
        return make_response('delete your data', 401, {'www-authenticate': ' basic realm="login required"'})


@app.route('/update', methods=['PUT'])
@token_required
def update(*args):
    user_type = args[1]
    current_user = args[0]
    if "email" in request.json:
        if user_type == 'admin':
            data = request.get_json()
            email = data['email']
            role_type = data['role_type']
            if role_type == 'teacher' or role_type == 'admin':
                cur.execute("SELECT * FROM teacher WHERE email = %s", [email])
            else:
                cur.execute("SELECT * FROM student WHERE email = %s", [email])

            user_data = cur.fetchone()
            if user_data:
                if role_type == 'admin':
                    role_type = 'teacher'
                if 'address' in request.json:
                    if r.exists(email):
                        r.hmset(email, {'address': request.json['address']})  # cache dict_object as redis hash
                        r.expire(email, 300)

                    cur.execute(f"UPDATE {role_type} SET address=%s WHERE email = %s", (request.json['address'], email))
                if 'name' in request.json:
                    if r.exists(email):
                        r.hmset(email, {'name': request.json['name']})
                        r.expire(email, 300)

                    cur.execute(f"UPDATE {role_type} SET name=%s WHERE email = %s", (request.json['name'], email))
                if 'mobile' in request.json:
                    if r.exists(email):
                        r.hmset(email, {'mobile': request.json['mobile']})  # cache dict_object as redis hash
                        r.expire(email, 300)

                    cur.execute(f"UPDATE {role_type} SET mobile=%s WHERE email = %s",
                                (request.json['mobile'], email))

                if not r.exists(email):
                    cur.execute(f"SELECT * FROM {role_type} where email='{email}'")
                    user_data = cur.fetchall()

                    for col in user_data:
                        hashed = col[2]
                        salt = col[3]
                        name = col[4]
                        address = col[5]
                        mobile = col[6]

                        dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                                       "mobile": mobile}

                        r.hmset(email, dict_object)  # cache dict_object as redis hash
                        r.expire(email, 300)

            else:
                return "please enter correct email id"
    else:
        if user_type == 'admin':
            user_type = "teacher"
        if 'address' in request.json:
            if r.exists(current_user):
                r.hmset(current_user, {'address': request.json['address']})
                r.expire(current_user, 300)

            cur.execute(f"UPDATE {user_type} SET address=%s WHERE email = %s", (request.json['address'], current_user))
        if 'name' in request.json:
            if r.exists(current_user):
                r.hmset(current_user, {'name': request.json['name']})
                r.expire(current_user, 300)

            cur.execute(f"UPDATE {user_type} SET name=%s WHERE email = %s", (request.json['name'], current_user))
        if 'mobile' in request.json:
            if r.exists(current_user):
                r.hmset(current_user, {'mobile': request.json['mobile']})
                r.expire(current_user, 300)

            cur.execute(f"UPDATE {user_type} SET mobile=%s WHERE email = %s", (request.json['mobile'], current_user))

        con.commit()
        if not r.exists(current_user):
            cur.execute(f"SELECT * FROM {user_type} where email='{current_user}'")
            user_data = cur.fetchall()

            for col in user_data:
                hashed = col[2]
                salt = col[3]
                name = col[4]
                address = col[5]
                mobile = col[6]

                dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                               "mobile": mobile}

                r.hmset(current_user, dict_object)  # cache dict_object as redis hash
                r.expire(current_user, 300)

    return make_response('update your data', 401, {'www-authenticate': ' basic realm="login required"'})


@app.route('/get_data')
@token_required
def show_data(*args):
    user_type = args[1]
    current_user = args[0]

    if r.exists(current_user):
        name = str(r.hmget(current_user, "name"))
        address = str(r.hmget(current_user, "address"))
        mobile = str(r.hmget(current_user, "mobile"))
        data = {
            "name": name[3:len(name) - 2],
            "address": address[3:len(address) - 2],
            "contact_number": mobile[3:len(mobile) - 2]
        }
        r.expire(current_user, 300)

        return jsonify({'information1': data})

    else:
        if user_type == 'teacher' or user_type == 'admin':
            cur.execute("SELECT * FROM teacher WHERE email = %s", [current_user])
        else:
            cur.execute("SELECT * FROM student WHERE email = %s", [current_user])

        user_data = cur.fetchall()

        data = []
        for col in user_data:
            hashed = col[2]
            salt = col[3]
            name = col[4]
            address = col[5]
            mobile = col[6]

            data = {
                'name': name,
                'address': address,
                'contact_number': mobile
            }
            dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                           "mobile": mobile}

            r.hmset(current_user, dict_object)  # cache dict_object as redis hash
            r.expire(current_user, 300)

    return jsonify({'information': data})


@app.route('/teachersdata', methods=['GET'])
@token_required
def get_teachers_data(*args):
    cur.execute("SELECT * FROM teacher")
    user_data = cur.fetchall()
    data = []
    for col in user_data:
        hashed = col[2]
        email = col[1]
        salt = col[3]
        name = col[4]
        address = col[5]
        mobile = col[6]
        if r.exists(email):
            name = str(r.hmget(email, "name"))
            address = str(r.hmget(email, "address"))
            mobile = str(r.hmget(email, "mobile"))
            data.append({
                "name": name[3:len(name) - 2],
                "address": address[3:len(address) - 2],
                "contact_number": mobile[3:len(mobile) - 2]
            })
            r.expire(email, 300)
        else:

            dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                           "mobile": mobile}
            r.hmset(email, dict_object)
            r.expire(email, 300)
            data.append({
                'name': name,
                'address': address,
                'contact_number': mobile
            })

    return jsonify({'information1': data})


@app.route('/studentsdata', methods=['GET'])
@token_required
def get_students_data(*args):
    cur.execute("SELECT * FROM student")
    user_data = cur.fetchall()
    data = []
    for col in user_data:
        hashed = col[2]
        email = col[1]
        salt = col[3]
        name = col[4]
        address = col[5]
        mobile = col[6]
        if r.exists(email):
            name = str(r.hmget(email, "name"))
            address = str(r.hmget(email, "address"))
            mobile = str(r.hmget(email, "mobile"))
            data.append({
                "name": name[3:len(name) - 2],
                "address": address[3:len(address) - 2],
                "contact_number": mobile[3:len(mobile) - 2]
            })
            r.expire(email, 300)
        else:

            dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                           "mobile": mobile}
            r.hmset(email, dict_object)
            r.expire(email, 300)
            data.append({
                'name': name,
                'address': address,
                'contact_number': mobile
            })

    return jsonify({'information': data})


@app.route('/teacher_login', methods=['POST'])
def teacher_login():
    return login('teacher')


@app.route('/admin_login', methods=['POST'])
def admin_login():
    return login('admin')


@app.route('/student_login', methods=['POST'])
def login(user_type='student'):
    data = request.get_json()
    email = data['email']
    password = data['password']

    if r.exists(email):
        hashed = r.hmget(email, "password")
        salt = r.hmget(email, "salt")
        print("----------------login--------------")
        salt = str(salt)
        salt = salt[3:len(salt) - 2]
        if '\\' in salt:
            n = salt.index('\\')
            salt = salt[0:n] + salt[n + 1:len(salt)]

        user_pass = password + salt
        user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

        hashed = str(hashed)
        hashed = hashed[3:len(hashed) - 2]

        if hashed == user_pass:
            token = jwt.encode({'user': email, 'user_type': user_type,
                                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=10)},
                               app.config['SECRET_KEY'], algorithm="HS256")
            r.expire(email, 300)

            return jsonify({'token': token})
        return "enter right password"
    else:
        if user_type == 'admin' or user_type == 'teacher':
            sql = f"select * from teacher where email='{email}'"

        else:
            sql = f"select * from student where email='{email}'"

        cur.execute(sql)
        row = cur.fetchone()
        if row is not None:

            hashed = row[2]
            salt = row[3]
            user = row[1]
            name = row[4]
            address = row[5]
            mobile = row[6]

            user_pass = password + salt
            user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

            if hashed == user_pass:
                token = jwt.encode({'user': user, 'user_type': user_type,
                                    'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=10)},
                                   app.config['SECRET_KEY'], algorithm="HS256")
                dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                               "mobile": mobile}

                r.hmset(email, dict_object)  # cache dict_object as redis hash
                r.expire(email, 600)

                return jsonify({'token': token})
            else:
                return jsonify({'message': 'enter right password'})

        return jsonify({'message': 'enter right id or user does not exists'})


@app.route('/register', methods=['POST'])
def register(*args):
    data = request.get_json()
    email = data['email']
    password = data['password']
    name = data['name']
    address = data['address']
    mobile = data['mobile']
    role_type = data['role_type']

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    if role_type == 'student':
        if r.exists(email):
            return "user already register"

        else:
            dict_object = {"password": hashed, "salt": salt_value, "name": name, "address": address, "mobile": mobile}

            r.hmset(email, dict_object)  # cache dict_object as redis hash
            r.expire(email, 300)
            try:
                sql = "INSERT INTO student (email,password,salt,name,address,mobile) VALUES(%s,%s,%s,%s,%s,%s)"
                cur.execute(sql, (email, hashed, salt_value, name, address, mobile))
            except Exception as e:
                print(e)
                r.delete(email)
                return "user already register"

    else:
        if r.exists(email):
            return "user already register"
        else:
            dict_object = {"password": hashed, "salt": salt_value, "name": name, "address": address, "mobile": mobile,
                           }
            r.hmset(email, dict_object)  # cache dict_object as redis hash
            r.expire(email, 300)
            try:
                sql = "INSERT INTO teacher (email,password,salt,name,address,mobile,role_type) VALUES(%s,%s,%s,%s,%s," \
                      "%s,%s) "
                cur.execute(sql, (email, hashed, salt_value, name, address, mobile, role_type))
            except Exception as e:
                print(e)
                r.delete(email)
                return "user already register"

    con.commit()
    return f"{name} register successfully"


@app.route('/update_password', methods=["POST", "GET"])
@token_required
def update_password(current_user, user_type):
    data = request.get_json()
    password = data['password']
    new_password = data['new_password']
    if user_type == 'admin' or user_type == 'teacher':
        sql = f"select * from teacher where email='{current_user}'"

    else:
        sql = f"select * from student where email='{current_user}'"

    cur.execute(sql)
    row = cur.fetchone()
    if row is not None:

        hashed = row[2]
        salt = row[3]
        name = row[4]
        address = row[5]
        mobile = row[6]

        user_pass = password + salt
        # hashed = row[2]
        user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

        if hashed == user_pass:
            new_password = new_password + salt
            hashed = hashlib.sha256(new_password.encode()).hexdigest()

            if user_type == 'teacher' or user_type == 'admin':
                if r.exists(current_user):
                    r.hmset(current_user, {'password': hashed})
                    r.expire(current_user, 300)

                sql = f"UPDATE teacher SET password ='{hashed}' WHERE email='{current_user}'"
            else:
                if r.exists(current_user):
                    r.hmset(current_user, {'password': hashed})
                    r.expire(current_user, 300)

                sql = f"UPDATE student SET password ='{hashed}' WHERE email='{current_user}'"

            cur.execute(sql)

            con.commit()
            if not r.exists(current_user):
                dict_object = {"password": hashed, "salt": salt, "name": name, "address": address,
                               "mobile": mobile,
                               }
                r.hmset(current_user, dict_object)  # cache dict_object as redis hash
                r.expire(current_user, 300)
            count = cur.rowcount
            print(count, "Record Updated successfully ")

    return jsonify({'message': 'password update successfully'})


@app.route('/uploadmarks', methods=['POST'])
@token_required
def uploadmarks(*args):
    csv_file = request.files['file']
    csv_file = TextIOWrapper(csv_file, encoding='utf-8')
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        email, physics, math, chemistry = row
        sql = "INSERT INTO marks (email,physics,math,chemistry) VALUES (%s,%s,%s,%s)"
        cur.execute(sql, [email, physics, math, chemistry])
    con.commit()

    return "upload marks successfully"


@app.route('/checkmarks', methods=['POST'])
@token_required
def checkmarks(*args):
    user_type = args[1]
    current_user = args[0]
    redis_current_user = "marks_" + current_user
    if user_type == 'student':
        if r.exists(redis_current_user):
            chemistry = str(r.hmget(redis_current_user, "chemistry"))
            physics = str(r.hmget(redis_current_user, "physics"))

            math = str(r.hmget(redis_current_user, "math"))

            data = {
                'chemistry ': chemistry[3:len(chemistry) - 2],
                'physics ': physics[3:len(physics) - 2],
                'math': math[3:len(math) - 2]

            }
            r.expire(redis_current_user, 300)
            return jsonify({'information': data})
            # salt = salt[3:len(salt) - 2]
            # print(salt)

        cur.execute("SELECT * FROM marks WHERE email = %s", [current_user])
        print(current_user)
        user_data = cur.fetchall()
        data = []
        if user_data:
            for col in user_data:
                physics = col[2]
                math = col[3]
                chemistry = col[4]

                data.append({
                    'physics': col[2],
                    'math': col[3],
                    'chemistry': col[4]

                })
                dict_object = {"physics": physics, "math": math, "chemistry": chemistry
                               }
                r.hmset(redis_current_user, dict_object)  # cache dict_object as redis hash
                r.expire(redis_current_user, 300)

        else:
            return "marks is not available"
    else:
        return "unauthorized user"

    return jsonify({'information': data})


def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)
    for _ in range(6):
        password += secrets.choice(string_source)
    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
