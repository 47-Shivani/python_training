from flask import Flask, jsonify , render_template, request
import psycopg2
import bcrypt
import secrets
import string
import hashlib
import psycopg2.extras
from psycopg2 import IntegrityError

app = Flask(__name__)

students=[]
con = psycopg2.connect(database="flaskapi", user="postgres", password="root", host="127.0.0.1", port="5432")
cur = con.cursor()


@app.route('/register', methods=['POST'])
def add_student():
    data = request.get_json()
    email = data['email']
    password = data['password']
    name = data['name']
    address = data['address']
    mobile = data['mobile']

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()

    sql = "INSERT INTO students (email,password,salt,name,address,mobile) VALUES(%s,%s,%s,%s,%s,%s)"
    cur.execute(sql, (email, new_pass, salt_value, name, address, mobile))
    con.commit()

    return f"{name} register successfully"



@app.route('/')
def index():
    cur.execute("select * from flasktable")
    students1 = cur.fetchall()
    for row in students1:

        student = {
            'id': row[0],
            'name': row[1],
            'email': row[2],
            'contact_number': row[3],
            'address': row[4]
        }
        students.append(student)
    return "hello student"


@app.route("/students",methods=['GET'])
def get():
   return jsonify({'Students':students})

@app.route('/students/<int:id>',methods=['GET'])
def get_Student(id):
    return  jsonify({'student':students[id]})

@app.route('/add')
def add_Student():
    return render_template('StudentsForm.html')
@app.route('/', methods=['POST'])
def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)
    for _ in range(6):
        password += secrets.choice(string_source)
    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password



if __name__ == '__main__':
   app.run(debug = True)


